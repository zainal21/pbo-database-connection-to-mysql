/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database_connection_zainal;

/**
 *
 * @author zainal
 */
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database_connection_Zainal {

  
    
//    set static params db connetion
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/db_sekolah";
    static final String DB_USER = "root";
    static final String DB_PASSWORD="";

//     set obj conn
     static Connection conn;
     static Statement stmt;
     static ResultSet rs;
     
     
    public static void main(String[] args) throws ClassNotFoundException {
        try {
            Class.forName(JDBC_DRIVER); // set driver conn
             
            //set created connection
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // set obj connetion
            stmt = conn.createStatement();
            
            // created query from database
            String _sql = "SELECT * FROM siswa";
            
            //executed query
            rs = stmt.executeQuery(_sql);
            
            //loop result from executed query
            while(rs.next()){
                System.out.println("NIS : " + rs.getInt("NIS"));
                System.out.println("NAMA : : " + rs.getString("Nama"));
                System.out.println("Alamat : " + rs.getString("Alamat"));
                System.out.println("-----------------------------------");
            }
            stmt.close();
            conn.close();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        
    }
    
}
